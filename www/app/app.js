angular.module('demoApp', [
    'ngRoute',
    'controllerDemo'
  ])
  .config(function($routeProvider) {
    $routeProvider.when("/controllerDemo", {
      templateUrl: "app/controllerDemo/controllerDemo.html",
      controller: "NameOneCtrl",
      controllerAs: "n1"
    }).otherwise({
      redirectTo: "/controllerDemo"
    })
  });