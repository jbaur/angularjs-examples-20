angular.module("controllerDemo", [])
  .controller('NameOneCtrl', function() {
    this.name = {
      first: "John"
    };

    this.instructors = [
      {
        first: "John",
        last: "Baur"
      },
      {
        first: "Paul",
        last: "Spears"
      },
      {
        first: "Bill",
        last: "Odom"
      }
    ];
  })
  .controller('NameTwoCtrl', function() {
    this.name = {
      first: "Paul"
    };
  });